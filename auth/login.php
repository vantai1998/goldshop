<!-- Page content -->
<div class="container mt--8 pb-5">
  <div class="row justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary border-0 mb-0">
        <div class="card-body px-lg-5 py-lg-5">
          <!-- login -->
          <form id="val_logForm" enctype="multipart/form-data">
            <!-- Email -->
            <div class="form-group">
              <label class="form-control-label" for="lg_email">
                <i class="ni ni-email-83"></i> Tài khoản
              </label>
              <input class="form-control" placeholder="Email" type="text" name="lgEmail" id="lg_email" required data-parsley-trigger="keyup">
            </div>

            <!-- Password -->
            <div class="form-group">
              <label class="form-control-label" for="lg_password">
                <i class="ni ni-lock-circle-open"></i> Mật khẩu
              </label>
              <input class="form-control" placeholder="Password" type="password" name="lgPassword" id="lg_password" required  data-parsley-trigger="keyup">
            </div>
            <div class="text-center">
              <button type="submit" name="submit" id="lg_submit" class="btn btn-primary my-4">
                <i class="fas fa-hourglass autoLoadding" id="autoLoaddingLogin" style="display: none;"></i>
                <span id="loginNow">ĐĂNG NHẬP</span>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

