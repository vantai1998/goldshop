<?php // GET connect database
function query_select($sql)
{ 
	$table = array();
	try {
		include 'connect.php';
		$lg = $conn->prepare($sql);
		$lg->execute();
		$table = $lg;
		$conn = null;
	} catch (PDOException $e) {
		echo "Connection failed: " . $e->getMessage();
	}
	return $table;
}
?>