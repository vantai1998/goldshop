<!-- open session -->
<?php session_start(); ?>

<!-- function -->
<?php include("functions.php"); ?>

<?php // check login
if(isset($_SESSION['isLoged']))
{
  if($_SESSION['isLoged']){
    header("location:admin/404.php");
  }
}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
	<meta name="author" content="Creative Tim">
	<title>GOLDSHOP</title>
	<!-- Favicon -->
	<link rel="icon" href="assets/admin/img/brand/favicon.png" type="image/png">
	<!-- Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
	<!-- Icons -->
	<link rel="stylesheet" href="assets/admin/vendor/nucleo/css/nucleo.css" type="text/css">
	<link rel="stylesheet" href="assets/admin/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
	<!-- Argon CSS -->
	<link rel="stylesheet" href="assets/admin/css/argon.css?v=1.2.0" type="text/css">
	<!-- Style format -->
	<link rel="stylesheet" href="assets/style.css" type="text/css">
</head>

<body class="bg-default">
	<!-- Main content -->
	<div class="main-content">
		<!-- Header -->
		<div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
			<div class="container">
				<div class="header-body text-center mb-2">
					<div class="row justify-content-center">
						<div class="col-xl-5 col-lg-6 col-md-8 px-5">
							<h1 class="text-white">QUẢN LÝ KHÁCH HÀNG</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="separator separator-bottom separator-skew zindex-100">
				<svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
					<polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
				</svg>
			</div>
		</div>

		<!-- main -->
		<?php include("auth/login.php"); ?>

	</div>

	<!-- Argon Scripts -->
	<!-- Core -->
	<script src="assets/admin/vendor/jquery/dist/jquery.min.js"></script>
	<script src="assets/admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="assets/admin/vendor/js-cookie/js.cookie.js"></script>
	<script src="assets/admin/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
	<script src="assets/admin/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
	<script src="assets/sweetalert2@8.js"></script>
	<!-- Validate From Res -->
	<script src="assets/parsley.js"></script>
	<!-- Validate Form -->
	<script src="assets/validate.js"></script>
	<!-- Argon JS -->
	<script src="assets/admin/js/argon.js?v=1.2.0"></script>
</body>

</html>