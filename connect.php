<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$options = array(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
	try{
		$conn = new PDO("mysql:host=$servername;dbname=db_goldshop", $username, $password,$options);
	} catch (PDOException $e) {
		echo "Kết nối thất bại: " . $e->getMessage();
	}
?>