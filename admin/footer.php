  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="./../assets/admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="./../assets/admin/vendor/js-cookie/js.cookie.js"></script>
  <script src="./../assets/admin/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="./../assets/admin/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Validate From Res -->
  <script src="./../assets/parsley.js"></script>
  <!-- Optional JS -->
  <script src="./../assets/admin/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="./../assets/admin/vendor/chart.js/dist/Chart.extension.js"></script>
  <!-- sweetalert -->
  <script src="./../assets/sweetalert2@8.js"></script>
  <!-- Argon JS -->
  <script src="./../assets/admin/js/argon.js?v=1.2.0"></script>
  <!-- Validate Form -->
  <script src="./../assets/validate.js"></script>
  <!-- Funtion -->
  <script src="./../assets/function.js"></script>
  <!-- Bin data chart -->
  <script src="./../assets/chart.js"></script>
  </body>

  </html>
