<?php session_start(); ?>

<!-- Connect database -->
<?php include('../../connect.php'); ?>

<!-- Function select -->
<?php include '../../functions.php'; ?>

<!-- Search New -->
<?php
if(isset($_POST['search'])) {

	$output = '';
	$search = $_POST['search'];
    $table = query_select("SELECT customer.CustomerID, customer.CustomerName, customer.CustomerPhoneNumber, customer.CustomerAddress, gift.GiftDate FROM customer, gift WHERE customer.CustomerID = gift.CustomerID AND customer.CustomerPhoneNumber LIKE '%".$search."%' GROUP BY customer.CustomerID ORDER BY gift.GiftDate");
    $count = $table->rowCount();

	$output = '
	<table class="table align-items-center table-flush">
        <thead class="thead-light">
            <tr>
                <th scope="col" class="sort" data-sort="name">STT</th>
                <th scope="col" class="sort" data-sort="budget">TÊN KHÁCH</th>
                <th scope="col" class="sort" data-sort="status">SỐ ĐIỆN THOẠI</th>
                <th scope="col" class="sort" data-sort="completion">ĐỊA CHỈ</th>
                <th scope="col" class="sort" data-sort="completion">NGÀY NHẬN</th>
                <th scope="col" class="sort" data-sort="completion">TRẠNG THÁI</th>
            </tr>
        </thead>
    ';
	if($count > 0) {
		foreach ($table as $row) {
            $CustomerID = $row['CustomerID'];
            $checkReceiveGift = query_select("SELECT customer.CustomerID, gift.GiftDate FROM customer, gift WHERE customer.CustomerID = gift.CustomerID AND customer.CustomerID = $CustomerID");
            $output .= '
            <tbody class="list">
                <tr>
                    <td class="budget">
                        '.$row['CustomerID'].'
                    </td>
                    <td class="budget">
                        '.$row['CustomerName'].'
                    </td>
                    <td class="budget">
                        '.$row['CustomerPhoneNumber'].'
                    </td>
                    <td class="budget">
                        '.$row['CustomerAddress'].'
                    </td>
                    <td class="budget">
                       DD
                    </td>
                    <td class="budget">
                        '.
                            ($checkReceiveGift->rowCount() > 0 ? '<span class="text-success">ĐÃ TẶNG QUÀ</span>' : '<a href="#" data-toggle="modal" data-target="#modal-giveAway" class="give-away" customer_id="'.$row["CustomerID"].'">
                            <button type="button" class="btn btn-sm btn-outline-info">TẶNG QUÀ</button></a>')
                        .'
                    </td>
                </tr>
            </tbody>
			';
		}
	} else {
		$output .= '
			<tbody class="list">
	            <tr>
	              <td class="text-center" colspan="6">
	                <div class="media-body">
	                  <span class="name text-sm">Chưa có thông tin trên hệ thống. 
					  </span>
						<a href="?page=add-news">
							<button type="button" class="btn btn-sm btn-outline-primary">TẠO MỚI THÔNG TIN KHÁCH</button>
						</a>
	                </div>
	              </td>
	            </tr>
          	</tbody>
		';
	}
	$output .= '</table>';
	echo $output;
}
?>