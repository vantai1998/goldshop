<?php session_start(); ?>
<?php include('../../connect.php'); //Connect database ?>
<?php include '../../functions.php'; //Function select ?>

<?php //Search New
if(isset($_POST['search'])) {

	$output = '';
	$search = $_POST['search'];
	$table = query_select("SELECT * FROM customer WHERE CustomerPhoneNumber LIKE '%".$search."%' ");
	$count = $table->rowCount();
	$output = '
	<table class="table align-items-center table-flush">
        <thead class="thead-light">
          <tr>
            <th scope="col" class="sort" data-sort="name">STT</th>
            <th scope="col" class="sort" data-sort="budget">TÊN KHÁCH</th>
            <th scope="col" class="sort" data-sort="status">SỐ ĐIỆN THOẠI</th>
            <th scope="col" class="sort" data-sort="completion">ĐỊA CHỈ</th>
            <th scope="col" class="sort text-center" data-sort="completion">CHI TIẾT</th>
          </tr>
        </thead>
    ';
	if($count > 0) {
		foreach ($table as $row) {
			$output .= '
				<tbody class="list">
                	<tr>
                    	<td class="budget">
                    		'.$row['CustomerID'].'
                    	</td>
	                    <td class="budget">
	                      '.$row['CustomerName'].'
	                    </td>
	                    <td>
	                      '.$row['CustomerPhoneNumber'].'
	                    </td>
	                    <td>
	                      '.$row['CustomerAddress'].'
						</td>
	                    <td class="text-center">
	                      <a href="?page=detailnew&&idCustomer='.$row['CustomerID'].'">
	                        <button type="button" class="btn btn-sm btn-outline-info"> 
							CẬP NHẬT THÊM ĐƠN HÀNG
	                        </button>
	                      </a>
	                    </td>
                	</tr>
                </tbody>
			';
		}
	} else {
		$output .= '
			<tbody class="list">
	            <tr>
	              <td class="text-center" colspan="6">
	                <div class="media-body">
	                  <span class="name text-sm">Chưa có thông tin trên hệ thống. 
					  </span>
						<a href="?page=add-news">
							<button type="button" class="btn btn-sm btn-outline-primary">TẠO MỚI THÔNG TIN KHÁCH</button>
						</a>
	                </div>
	              </td>
	            </tr>
          	</tbody>
		';
	}
	$output .= '</table>';
	echo $output;
}
?>

<?php //ADD
if(isset($_POST['sodienthoai']))
{
	try {
		$sodienthoai = $_POST['sodienthoai'];
		if(isset($_POST['tenkhachhang'])){
			$tenkhachhang = ucwords($_POST['tenkhachhang']);
		}
		if(isset($_POST['diachikhachhang'])){
			$diachikhachhang = ucwords($_POST['diachikhachhang']);
		}
		$sotiengiaodich = $_POST['sotiengiaodich'];
		$sotienlai = $_POST['sotienlai'];
		$motadichvu = $_POST['motadichvu'];
		$ngaytaodonhang = date("Y/m/d H:i:s");
		$loaigiaodich = $_POST['loaidichvu'];
		$idCustomer = 0;

		$checkIDCustomer = $_POST['checkIDCustomer'];
		if($checkIDCustomer == null){
			if(!isset($_POST["id-customer"])){ //TẠO MỚI
				$sql = "INSERT INTO customer (CustomerName, CustomerPhoneNumber, CustomerAddress) VALUES ('$tenkhachhang','$sodienthoai','$diachikhachhang')";
				$conn->exec($sql);
				// record id add
				$lastID = $conn->lastInsertId();
				$idCustomer = $lastID;
			} else { 
				$idCustomer = $_POST["id-customer"];
			}
			$arr = array(
				'idCustomer' => $idCustomer,
			);
			echo json_encode($arr);
			$query = "INSERT INTO orders (CustomerID, OrderDate, OrderTotal, OrderStatus, OrderNote, OrderInterestRate) VALUES ('$idCustomer','$ngaytaodonhang', '$sotiengiaodich', '$loaigiaodich', '$motadichvu', '$sotienlai')";
			$conn->exec($query);
		} else {
			$query = "INSERT INTO orders (CustomerID, OrderDate, OrderTotal, OrderStatus, OrderNote, OrderInterestRate) VALUES ('$checkIDCustomer','$ngaytaodonhang', '$sotiengiaodich', '$loaigiaodich', '$motadichvu', '$sotienlai')";
			$conn->exec($query);
		}
	} catch (PDOException $e) {
		echo "Connection failed: " . $e->getMessage();
	}
}
?>

<?php //Search New
if(isset($_POST['searchPhone'])) {

	$searchPhone = $_POST['searchPhone'];
	$tablePhone = query_select("SELECT * FROM customer WHERE CustomerPhoneNumber LIKE '%".$searchPhone."%' ");
	$countPhone = $tablePhone->rowCount();
	$arr = [];
	if($countPhone > 0) {
		foreach ($tablePhone as $row) {
			$arr = array(
				'CustomerName' => $row['CustomerName'],
				'CustomerID' => $row['CustomerID'],
				'CustomerAddress' => $row['CustomerAddress'],
			);
		}
	} else {
		$arr = array(
			'CustomerName' => NULL,
			'CustomerID' => NULL,
			'CustomerAddress' => NULL,
		);
	}
	echo json_encode($arr);
}
?>