<?php session_start(); ?>

<!-- Connect database -->
<?php include('../../connect.php'); ?>

<!-- Function select -->
<?php include '../../functions.php'; ?>

<!-- Search New -->
<?php
if(isset($_POST['search'])) {

	$output = '';
	$search = $_POST['search'];
    $table = query_select("SELECT customer.CustomerID, customer.CustomerName, customer.CustomerPhoneNumber, customer.CustomerAddress, SUM(orders.OrderTotal) AS OrderTotal, SUM(orders.OrderInterestRate) AS OrderInterestRate FROM customer, orders WHERE customer.CustomerID = orders.CustomerID AND customer.CustomerPhoneNumber LIKE '%".$search."%' GROUP BY customer.CustomerID ORDER BY OrderInterestRate DESC");
    $count = $table->rowCount();
    
    // lấy cài đặt (số tiền tối thiểu để nhận quà)
    $settingRs = query_select("SELECT GiftCondition FROM Settings;");
    $settings = [];
    foreach($settingRs as $r){
      $settings = $r;
    }
    $pAdminLevel = 1;

	$output = '
	<table class="table align-items-center table-flush">
        <thead class="thead-light">
            <tr>
                <th scope="col" class="sort" data-sort="name">STT</th>
                <th scope="col" class="sort" data-sort="budget">TÊN KHÁCH</th>
                <th scope="col" class="sort" data-sort="status">SỐ ĐIỆN THOẠI</th>
                <th scope="col" class="sort" data-sort="completion">SỐ TIỀN MUA HÀNG</th>
                <th scope="col" class="sort" data-sort="completion">SỐ TIỀN CẦM ĐỒ</th>
                <th scope="col" class="sort" data-sort="completion">SỐ TIỀN CHUYỂN ĐỔI</th>
                <th scope="col" class="sort" data-sort="completion">SỐ TIỀN LÃI</th>
                <th class="text-center" scope="col" class="sort" data-sort="completion">TẶNG QUÀ</th>
            </tr>
        </thead>
    ';
	if($count > 0) {
		foreach ($table as $row) {
            $customerID = $row["CustomerID"];
            // lấy số tiền mua hàng
            $temp = query_select("SELECT SUM(orders.OrderTotal) AS OrderTotal FROM customer, orders WHERE customer.CustomerID = orders.CustomerID AND customer.CustomerID = $customerID AND orders.OrderStatus = 1 LIMIT 1;");
                $totalBuy = [];
                foreach($temp as $r){
                  $totalBuy = $r;
            }
            // lấy số tiền cầm đồ
            $temp = query_select("SELECT SUM(orders.OrderTotal) AS OrderTotal FROM customer, orders WHERE customer.CustomerID = orders.CustomerID AND customer.CustomerID = $customerID AND orders.OrderStatus = 2 LIMIT 1;");
                $totalPawn = [];
                foreach($temp as $r){
                  $totalPawn = $r;
            }
            // lấy số tiền chuyển đổi
            $temp = query_select("SELECT SUM(orders.OrderTotal) AS OrderTotal FROM customer, orders WHERE customer.CustomerID = orders.CustomerID AND customer.CustomerID = $customerID AND orders.OrderStatus = 3 LIMIT 1;");
                $totalExchange = [];
                foreach($temp as $r){
                  $totalExchange = $r;
            }

            // kiểm tra đã nhận quà
            $checkReceivedGift = query_select("SELECT CustomerID FROM gift WHERE CustomerID=$customerID");

            $output .= '
            <tbody class="list">
                <tr>
                    <td class="budget">
                        '.$row['CustomerID'].'
                    </td>
                    <td class="budget">
                        '.$row['CustomerName'].'
                    </td>
                    <td class="budget">
                        '.$row['CustomerPhoneNumber'].'
                    </td>
                    <td class="budget">
                        '.number_format($totalBuy['OrderTotal'] * 1000,0,".",",") . " Đ".'
                    </td>
                    <td class="budget">
                        '.number_format($totalPawn['OrderTotal'] * 1000,0,".",",") . " Đ".'
                    </td>
                    <td class="budget">
                        '.number_format($totalExchange['OrderTotal'] * 1000,0,".",",") . " Đ".'
                    </td>
                    <td class="budget">
                        '.number_format($row['OrderInterestRate'],0,".",",") . " ₫".'
                    </td>
                    <td class="budget text-center">
                        '.
                        (
                            ($checkReceivedGift->rowCount() > 0) ?
                            '<span class="text-success">ĐÃ NHẬN QUÀ</span>'
                            :
                            ($row['OrderInterestRate'] < $settings["GiftCondition"] ?
                            '<span class="text-danger" title="Tổng tiền giao dịch phải từ '.number_format($settings["GiftCondition"],0,".",",") .' ₫">KHÔNG ĐỦ ĐIỀU KIỆN </br> NHẬN QUÀ</span>'
                            :
                            '<a href="#" data-toggle="modal" data-target="#modal-giveAway" class="give-away" customer_id="'.$row["CustomerID"].'">
                            <button type="button" class="btn btn-sm btn-outline-info"> 
                            TẶNG QUÀ
                            </button>
                          </a>')
                        )
                        .'
                    </td>
                </tr>
            </tbody>
			';
		}
	} else {
		$output .= '
			<tbody class="list">
	            <tr>
	              <td class="text-center" colspan="6">
	                <div class="media-body">
	                  <span class="name text-sm">Chưa có thông tin trên hệ thống. 
					  </span>
						<a href="?page=add-news">
							<button type="button" class="btn btn-sm btn-outline-primary">TẠO MỚI THÔNG TIN KHÁCH</button>
						</a>
	                </div>
	              </td>
	            </tr>
          	</tbody>
		';
	}
	$output .= '</table>';
	echo $output;
}
?>