<div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Navbar links -->
                <ul class="navbar-nav align-items-center  ml-md-auto ">
                    <li class="nav-item d-xl-none">
                        <!-- Sidenav toggler -->
                        <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin"
                            data-target="#sidenav-main">
                            <div class="sidenav-toggler-inner">
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                            </div>
                        </div>
                    </li>
                </ul>

                <!-- Profile Me -->
                <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
                    <li class="nav-item dropdown">
                        <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">

                            <div class="media align-items-center">
                                <span class="avatar avatar-sm rounded-circle">
                                    <img src="../assets/admin/img/brand/favicon.png" id="bin_val_avatarChange">
                                </span>
                                <div class="media-body  ml-2  d-none d-lg-block">
                                    <span class="mb-0 text-sm  font-weight-bold" id="bin_val_username_hd">QUẢN TRỊ
                                        VIÊN</span>
                                </div>
                            </div>

                        </a>
                        <div class="dropdown-menu  dropdown-menu-right ">
                            <div class="dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Xin chào !</h6>
                            </div>
                            <a href="#!" class="dropdown-item" data-toggle="modal" data-target="#modal-changePassword">
                                <i class="fas fa-exchange-alt"></i>
                                <span>Thay đổi mật khẩu</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#!" class="dropdown-item">
                                <?php
                if (isset($_POST['logOut'])) {
                  logout();
                }
                function logout() {
                  $_SESSION['isLoged'] = false;
                  $_SESSION['role_admin'] = false;
                  echo '<script> window.location = "../index.php"; </script>';
                }
                ?>
                                <form action="" method="POST">
                                    <button type="submit" name="logOut" class="btn btn-primary btn-sm btn-block"><i
                                            class="ni ni-user-run"></i> Đăng xuất</button>
                                </form>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- page pagination -->
    <?php
  if(isset($_GET['page'])){
    $page = $_GET['page'];
    switch($page){
      // list new
      case 'list-news': 
        include ("./modules/private/list-news/list-news.module.php");
        break;
      case 'detailnew': 
        include ("./modules/private/list-news/components/update-news/update-new.module.php");
        break;
      case 'add-news': 
        include ("./modules/private/list-news/components/add-news/add-new.module.php");
        break;
      // list gift 
      case 'list-gifts': 
        include ("./modules/private/list-gifts/list-gifts.module.php");
        break;
      // settings
      case 'settings': 
        include ("./modules/private/settings/settings.module.php");
        break;
    }
  } else {
    include("./modules/private/dashboard/dashboard.module.php");
  }
  ?>

</div>

<!-- Change Password -->
<?php include("./modules/private/change-password/change-password.module.php"); ?>