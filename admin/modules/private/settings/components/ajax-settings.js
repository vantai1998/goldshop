$(document).ready(function(){
	// submit form
	$('#setting_form').on('submit', function(event) {
		// $(this).hide();
		event.preventDefault();
		$.ajax({
			url: "./services/settings.services.php",
			method: "POST",
			data: new FormData(this),
			// dataType : 'json',
			contentType:false,
			cache:false,
			processData:false,
			beforeSend: function() {
				// alert('prepare');
				// $('#submit_changePassword').attr('disabled', 'disabled');
				$('#autoLoaddingSetting').show();
				$('#settingText').text('Đang tải...');
			},
			success: function(res) {     
				// $('#setting_form')[0].reset();
				// $('#setting_form').parsley().reset();
				$('#submit_setting').attr('disabled', false);
				$('#autoLoaddingSetting').hide();
				$('#settingText').text('LƯU THAY ĐỔI');
				Swal.fire({
					type: 'success',
					title: 'CÀI ĐẶT',
					text: 'Cài đặt thành công',
					showConfirmButton: false,
					timer: 1500,
					onClose: () => {
						location.reload();
					}
				});
			},
			error: (err) => {
				console.log("ERROR AJAX");
				console.log(err);
			}
		});
	});
});