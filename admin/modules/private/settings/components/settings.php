<!-- header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">CÀI ĐẶT</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="./">TRANG CHỦ</a></li>
              <li class="breadcrumb-item active" ara-current="page">CÀI ĐẶT</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- table -->
<div class="container-fluid mt--6">
  <!-- table list new -->
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <div class="row">
            <div class="col"><h3 class="mb-0">CÀI ĐẶT CÁC GIÁ TRỊ MẶC ĐỊNH</h3> </div>
            <div class="col text-right">
              <!-- <a href="?page=add-news">
                <button type="button" class="btn btn-sm btn-outline-primary">TẠO MỚI THÔNG TIN KHÁCH</button>
              </a> -->
            </div>
          </div>
        </div>
        <div class="card-body">
          <form id="setting_form" enctype="multipart/form-data">
              <input type="hidden" name="SettingID" value="<?php echo $data["SettingID"] ?>">
              <!-- <h6 class="heading-small text-muted mb-4">CÀI ĐẶT</h6> -->
              <div class="pl-lg-4">
                <?php 
                  $oldSettings = query_select("SELECT SettingID, GiftCondition, DeleteReceiveGiftTime FROM Settings");
                  $data = [];
                  foreach($oldSettings as $row){
                    $data = $row;
                  }
                ?>
                  <div class="row">
                      <!-- Số điện thoại khách hàng -->
                      <!-- <div class="col-lg-6">
                          <div class="form-group">
                            <label class="form-control-label nhomdichvu_text" for="type-service">XÓA DỮ LIỆU KHÁCH HÀNG ĐÃ NHẬN QUÀ</label>
                            <div class="input-group">
                                <input type="number" class="form-control"
                                    aria-label="Amount (to the nearest dollar)" id="type-service"
                                    name="DeleteReceiveGiftTime" required data-parsley-trigger="keyup"
                                    data-parsley-type="digits" placeholder="Nhập số ngày dữ liệu khách hàng đã nhận quà sẽ xóa" min="1" value="<?php //echo $data["DeleteReceiveGiftTime"] ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text showValueUnit">Ngày</span>
                                </div>
                            </div>
                          </div>
                      </div> -->

                      <!-- Tên khách hàng -->
                      <div class="col-lg-12 nhomdichvu" id="nhomdichvu">
                        <label class="form-control-label nhomdichvu_text">SỐ TIỀN LÃI TỐI THIỂU ĐỂ NHẬN QUÀ</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">VNĐ</span>
                            </div>
                            <input type="number" class="form-control"
                                aria-label="Amount (to the nearest dollar)" id="type-service"
                                name="GiftCondition" required data-parsley-trigger="keyup"
                                data-parsley-type="digits" placeholder="Nhập số tiền..." min="0" value="<?php echo $data["GiftCondition"]; ?>">
                            <div class="input-group-append">
                            <label class="input-group-text showValueUnit" for="type-service"><?php echo number_format($data["GiftCondition"],0,".","."); ?> đ</label>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>

              <!-- Button submit -->
              <div class="pl-lg-4"> <br>
                  <button type="submit" name="submit" id="submit_setting"
                      class="btn btn-primary btn-lg btn-block">
                      <i class="fas fa-hourglass autoLoadding" id="autoLoaddingSetting"
                          style="display: none;"></i>
                      <span id="settingText">LƯU THAY ĐỔI</span>
                  </button>
              </div>
          </form>
      </div>
      </div>
    </div>
  </div>

  <!-- Give away  -->
  <div class="modal fade" id="modal-giveAway" tabindex="-1" role="dialog" aria-labelledby="modal-giveAway" aria-hidden="true">
    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
      <div class="modal-content bg-gradient-info">
        <form id="give_away_form">
          <input type="hidden" id="customer_id" name="customer_id" value="0">
          <input type="hidden" id="note" name="note" value="">
          <div class="modal-header">
            <h6 class="modal-title" id="modal-title-giveAway">TẶNG QUÀ</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="py-3 text-center">
              <i class="fas fa-gift ni-3x"></i>
              <!-- <i class="fas fa-key ni-3x"></i> -->
            </div>
            
            <h3 class="text-center text-white">Tặng quà cho người này?</h3>
            
          </div>

          <div class="modal-footer">
            <button type="submit" name="submit_giveAway" id="submit_giveAway" class="btn btn-white">
              <i class="fas fa-hourglass autoLoadding" id="autoLoaddingUpGiveAway" style="display: none;"></i>
              <span id="updateNowGiveAway">OK</span>
            </button>
            <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Không</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Nguyễn Văn Tài</a>
        </div>
      </div>
    </div>
  </footer>
</div>