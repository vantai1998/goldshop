$(document).ready(function () {
	$('#validate_form').parsley();
	$('#validate_form').on('submit', function (event) {
		event.preventDefault();
		if ($('#validate_form').parsley().isValid()) {
			$.ajax({
				url: "./services/list-news.services.php",
				method: "POST",
				data: new FormData(this),
				dataType : 'json',
				contentType: false,
				cache: false,
				processData: false,
				beforeSend: function () {
					$('#submit_addNew').attr('disabled', 'disabled');
					$('#autoLoaddingAddNew').show();
					$('#addNowNew').text('Đang xử lý...');
				},
				success: function (data) {
					$('#validate_form')[0].reset();
					$('#validate_form').parsley().reset();
					$('#submit_addNew').attr('disabled', false);
					$('#autoLoaddingAddNew').hide();
					$('#addNowNew').text('TẠO MỚI ĐƠN HÀNG');
					window.location = "?page=detailnew&&idCustomer="+data.idCustomer;
					Swal.fire({
						type: 'success',
						title: 'TẠO MỚI ĐƠN HÀNG',
						text: 'Bạn vừa thêm một đơn hàng thành công',
						showConfirmButton: false,
						timer: 1500
					});
				}
			});
		}
	});

	// Search List New
	$('#input-sodienthoai').keyup(function () {
		var search = $(this).val();
		if (search != '') {
			$.ajax({
				url: "./services/list-news.services.php",
				method: "POST",
				data: { searchPhone: search },
				dataType: 'json',
				success: function (res) {
					if (res.CustomerName == null) {
						$('#input-tenkhachhang').removeAttr('disabled');
					} else {
						$('#input-tenkhachhang').attr('disabled', 'disabled');
					}
					if (res.CustomerAddress == null) {
						$('#input-diachikhachhang').removeAttr('disabled');
					} else {
						$('#input-diachikhachhang').attr('disabled', 'disabled');
					}
					$('#input-tenkhachhang').val(res.CustomerName)
					$('#input-diachikhachhang').val(res.CustomerAddress)
					$('#input-idcustomer').val(res.CustomerID)





				}
			});
		}
	});

});