<?php 
    $customerInfo = [];
    if(isset($_GET['idCustomer'])){
        $idCustomer = $_GET['idCustomer'];
        $customer = query_select("SELECT CustomerName, CustomerPhoneNumber, CustomerAddress FROM Customer WHERE CustomerID = $idCustomer");
        foreach($customer as $r){
            $customerInfo = $r;
        }
    }
?>

<!-- header form background -->
<div class="header pb-6 d-flex align-items-center"
    style="min-height: 200px; background-image: url(); background-size: cover; background-position: center top;">
    <span class="mask bg-gradient-default opacity-8"></span>
</div>

<!-- form submit -->
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <!-- top form -->
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">THÊM MỚI THÔNG TIN KHÁCH HÀNG</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="?page=<?php echo isset($idCustomer) ? "detailnew&&idCustomer=$idCustomer" : "list-news" ?>" class="btn btn-sm btn-primary">Trở lại</a>
                        </div>
                    </div>
                </div>

                <!-- main form -->
                <div class="card-body">
                    <form id="validate_form" enctype="multipart/form-data">
                        <?php if(isset($idCustomer)){ ?> 
                        <input type="hidden" name="id-customer" value="<?php echo $idCustomer; ?>">    
                        <?php } ?>
                        <input type="text" id="input-idcustomer" name="checkIDCustomer" style="display:none">
                        <h6 class="heading-small text-muted mb-4">THÊM KHÁCH HÀNG</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <!-- Số điện thoại khách hàng -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-sodienthoai">SỐ ĐIỆN THOẠI</label>
                                        <input type="text" id="input-sodienthoai" class="form-control"
                                            name="sodienthoai" value="<?php echo count($customerInfo) > 0 ? $customerInfo["CustomerPhoneNumber"] : "" ?>" placeholder="Nhập số điện thoại khách hàng..." required
                                            data-parsley-length=[10,10] data-parsley-trigger="keyup"
                                            data-parsley-type="digits" <?php echo isset($idCustomer) ? "readonly" : "" ?>>
                                    </div>
                                </div>

                                <!-- Tên khách hàng -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-tenkhachhang">TÊN KHÁCH
                                            HÀNG</label>
                                        <input type="text" id="input-tenkhachhang" class="form-control"
                                            name="tenkhachhang" value="<?php echo count($customerInfo) > 0 ? $customerInfo["CustomerName"] : "" ?>" placeholder="Nhập tên khách hàng..." required
                                            data-parsley-trigger="keyup" <?php echo isset($idCustomer) ? "readonly" : "" ?>>
                                    </div>
                                </div>

                                <!-- Địa chỉ khách hàng -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-dichikhachhang">ĐỊA CHỈ KHÁCH
                                            HÀNG</label>
                                        <input type="text" id="input-diachikhachhang" class="form-control"
                                            name="diachikhachhang" value="<?php echo count($customerInfo) > 0 ? $customerInfo["CustomerAddress"] : "" ?>" placeholder="Nhập địa chỉ khách hàng..." required
                                            data-parsley-trigger="keyup" <?php echo isset($idCustomer) ? "readonly" : "" ?>>
                                    </div>
                                </div>

                                <!-- Loại dịch vụ -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect2">LOẠI DỊCH VỤ</label>
                                        <select onchange="fun_showHide(this)" class="form-control"
                                            id="exampleFormControlSelect2" required data-parsley-trigger="keyup"
                                            name="loaidichvu">
                                            <option value="">-- Chọn Loại Dịch Vụ --</option>
                                            <option value="1">MUA MỚI</option>
                                            <option value="2">CẦM ĐỒ</option>
                                            <option value="3">CHUYỂN ĐỔI</option>
                                        </select>
                                        <script type="text/javascript">
                                        function fun_showHide(checkbox) {
                                            var valueType = checkbox.value;
                                            switch (valueType) {
                                                case '1':
                                                    $('#nhomdichvu').show();
                                                    $('.nhomdichvu_text').text("DỊCH VỤ MUA MỚI");
                                                    break;
                                                case '2':
                                                    $('#nhomdichvu').show();
                                                    $('.nhomdichvu_text').text("DỊCH VỤ CẦM ĐỒ");
                                                    break;
                                                case '3':
                                                    $('#nhomdichvu').show();
                                                    $('.nhomdichvu_text').text("DỊCH VỤ CHUYỂN ĐỔI");
                                                    break;
                                                default: $('#nhomdichvu').hide();
                                            }
                                        }
                                        </script>
                                    </div>
                                </div>

                                <!-- Nhóm dịch vụ -->
                                <div class="col-lg-12 nhomdichvu" id="nhomdichvu">
                                    <!-- MUA MỚI -->
                                    <div class="row">
                                        <div class="col-lg-6 form-group dvmuamoi">
                                            <label class="form-control-label nhomdichvu_text" for="type-service1"></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">VNĐ</span>
                                                </div>
                                                <input type="number" class="form-control"
                                                    aria-label="Amount (to the nearest dollar)" id="type-service"
                                                    name="sotiengiaodich" required data-parsley-trigger="keyup"
                                                    data-parsley-type="digits" placeholder="Nhập số tiền..." min="0">
                                                <div class="input-group-append">
                                                    <label class="input-group-text showValueUnit"  for="type-service"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 form-group dvmualai">
                                            <label class="form-control-label" for="sotienuocluong">SỐ TIỀN LÃI ƯỚC LƯỢNG</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">VNĐ</span>
                                                </div>
                                                <input type="number" class="form-control"
                                                    aria-label="Amount (to the nearest dollar)" id="type-service2"
                                                    name="sotienlai" required data-parsley-trigger="keyup"
                                                    data-parsley-type="digits" placeholder="Nhập số tiền lãi ước lượng..." min="0">
                                                <div class="input-group-append">
                                                    <label class="input-group-text showValueUnit"  for="type-service2"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">MÔ TẢ GIAO DỊCH</span>
                                            </div>
                                            <textarea class="form-control" aria-label="With textarea"
                                                name="motadichvu"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Button submit -->
                        <div class="pl-lg-4"> <br>
                            <button type="submit" name="submit" id="submit_addNew"
                                class="btn btn-primary btn-lg btn-block">
                                <i class="fas fa-hourglass autoLoadding" id="autoLoaddingAddNew"
                                    style="display: none;"></i>
                                <span id="addNowNew">TẠO MỚI ĐƠN HÀNG</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6">
                <div class="copyright text-center  text-lg-left  text-muted">
                    © 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative
                        Tim</a>
                </div>
            </div>
            <div class="col-lg-6">
                <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                    <li class="nav-item">
                        <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
</div>