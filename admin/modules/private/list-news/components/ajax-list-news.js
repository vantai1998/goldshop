$(document).ready(function(){
	// Search List New
	load_data();
	function load_data(res) {
		$.ajax({
			url:"./services/list-news.services.php",
			method:"POST",
			data:{ search:res },
			success:function(res) {
				$('#resultSearch').html(res);
			}
		});
	}
	$('#searchNew').keyup(function() {
		var search = $(this).val();
		if(search != '') {
			load_data(search);
			$('#tableDefaultSearchNew').hide();
		} else {
			load_data();
			$('#tableDefaultSearchNew').show();
		}
	});

});