<?php 
	include '../connect.php';
    $idCustomer = $_GET['idCustomer'];
    $tongtien = 0;
?>

<!-- header form background -->
<div class="header pb-6 d-flex align-items-center"
    style="min-height: 200px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
    <span class="mask bg-gradient-default opacity-8"></span>
</div>

<!-- detail news -->
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <!-- top form -->
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">TẤT CẢ ĐƠN HÀNG KHÁCH ĐÃ MUA</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="?page=list-news" class="btn btn-sm btn-primary">Trở lại</a>
                        </div>
                    </div>
                </div>

                <!-- main content -->
                <div class="card-body">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <h3 class="mb-0">Thông Tin Khách Hàng</h3>
                            <?php 
							$table = query_select("SELECT customer.CustomerName, customer.CustomerPhoneNumber, customer.CustomerAddress FROM customer WHERE customer.CustomerID = $idCustomer ");
							$count = $table->rowCount();
							if($count > 0){ 
								foreach ($table as $row) {
									?>
                            <button type="button" class="btn btn-default">
                                <span class="badge badge-danger">HỌ TÊN</span><br>
                                <span><?php echo $row['CustomerName']; ?></span>
                            </button>
                            <button type="button" class="btn btn-default">
                                <span class="badge badge-danger">SỐ ĐIỆN THOẠI</span><br>
                                <span><?php echo $row['CustomerPhoneNumber']; ?></span>
                            </button>
                            <button type="button" class="btn btn-default">
                                <span class="badge badge-danger">ĐỊA CHỈ</span><br>
                                <span><?php echo $row['CustomerAddress']; ?></span>
							</button>
							<a href="?page=add-news&&idCustomer=<?php echo $idCustomer; ?>" type="button" class="btn btn-primary">
								<span>THÊM ĐƠN HÀNG</span>
							</a>
                            <?php
								}
							}
							?>
							
                        </div>
                        <!-- Light table -->
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col" class="sort" data-sort="name">STT</th>
                                        <th scope="col" class="sort" data-sort="name">NGÀY MUA</th>
                                        <th scope="col" class="sort" data-sort="budget">LOẠI DỊCH VỤ</th>
                                        <th scope="col" class="sort" data-sort="status">GHI CHÚ</th>
                                        <th scope="col" class="sort" data-sort="status">SỐ TIỀN THANH TOÁN</th>
                                    </tr>
                                </thead>
                                <tbody class="list">
                                    <?php 
									$table = query_select("SELECT orders.OrderDate, orders.OrderStatus, orders.OrderNote, orders.OrderTotal FROM orders WHERE orders.CustomerID =$idCustomer ");
									$count = $table->rowCount();
									if($count > 0){
                                        $i = 0;
                                        $tongtien = 0;
										foreach ($table as $row) {
                                            $i++;
                                            $tongtien += $row['OrderTotal'];
											?>
                                    <tr <?php if($i%2 == 0) { echo 'style="background-color: #dee2e6;"'; } ?>>
                                        <th scope="row">
                                            <?php echo $i; ?>
                                        </th>
                                        <th scope="row">
                                        <?php echo date_format(date_create($row['OrderDate']),"d/m/Y"); ?>
                                        </th>
                                        <td class="budget">
                                            <?php
											if($row['OrderStatus'] == '1'){
												echo 'DV MUA MỚI';
											} else if($row['OrderStatus'] == 2){
												echo 'DV CẦM ĐỒ';
											} else {
												echo 'DV CHUYỂN ĐỔI';
											}
											?>
                                        </td>
                                        <td>
                                            <?php if($row['OrderNote'] == null) { echo '//'; } else {echo $row['OrderNote'];} ?>
                                        </td>
                                        <td>
                                            <?php echo number_format($row['OrderTotal'],0,".",",") . " ₫"; ?>
                                        </td>
									</tr>
                                    <?php
										}
									}
									?>
                                </tbody>
                            </table>
                        </div>

                        <!-- Card footer -->
                        <div class="card-footer py-4 text-center display-3">
                            Tổng Tiền: <?php echo number_format($tongtien,0,".",",") . " ĐỒNG"; ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <footer class="footer pt-0">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-lg-6">
                        <div class="copyright text-center  text-lg-left  text-muted">
                            © 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1"
                                target="_blank">Creative Tim</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                            <li class="nav-item">
                                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>