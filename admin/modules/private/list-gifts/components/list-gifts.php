<!-- header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">KHÁCH HÀNG</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="./">TRANG CHỦ</a></li>
              <li class="breadcrumb-item active" ara-current="page">DANH SÁCH TẶNG QUÀ</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- table -->
<div class="container-fluid mt--6">
  <!-- table list new -->
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header border-0">
          <div class="row">
            <div class="col"><h3 class="mb-0">DỮ LIỆU KHÁCH HÀNG NHẬN QUÀ</h3> </div>
            <div class="col text-right">
            </div>
          </div>
        </div>

        <!-- Show entries, Search-->
        <div class="card-header border-0">
          <div class="row fixSearchListNew">
            <!-- search -->
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
              <form method="POST" class="navbar-search navbar-search-light form-inline mr-sm-3" id="search-new">
                <div class="form-group mb-0">
                  <div class="input-group input-group-alternative input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                    <input class="form-control" placeholder="Tìm bằng số điện thoại" type="text" name="searchNew" id="searchNew">
                  </div>
                </div>
              </form>
            </div>

            <!-- Show entries -->
            <div class="showEntrie justify-content-end col-lg-6 col-md-6 col-sm-12 col-12 form-inline flex-2r">
              <script type="text/javascript">
                function fun_showEntries(select) {
                  var data = select.value;
                  <?php if(isset($_GET['id'])) { $idPage =  $_GET['id']; } else { $idPage = 1; } ?>
                  window.location = "?page=list-gifts&&showEntries="+ data+"&&id=<?php echo $idPage; ?>";
                  <?php
                  if(isset($_GET['showEntries'])){
                    $limit = $_GET['showEntries'];
                    $_SESSION['showEntries'] = $_GET['showEntries'];
                  } else {
                    $limit = 10; //show page default
                    $_SESSION['showEntries'] = 10;
                  }
                  ?>
                }
              </script>
              <form method="POST" enctype="multipart/form-data">
                Hiển thị &nbsp;
                <select class="form-control" id="showEntries" name="showEntries" onchange="fun_showEntries(this)">
                  <option value="<?php echo $limit; ?>"><?php echo $limit; ?></option>
                  <?php if($limit != 5){?>
                    <option value="5">5</option>
                  <?php }?>

                  <?php if($limit != 10){?>
                    <option value="10">10</option>
                  <?php }?>

                  <?php if($limit != 25){?>
                    <option value="25">25</option>
                  <?php }?>

                  <?php if($limit != 50){?>
                    <option value="50">50</option>
                  <?php }?>

                  <?php if($limit != 100){?>
                    <option value="100">100</option>
                  <?php }?>
                </select>
                &nbsp; dòng
              </form>
            </div>
          </div>
        </div>

        <!-- Light table -->
        <div class="table-responsive">
          <!-- Result search -->
          <div id="resultSearch"></div>
          
          <!-- Default Search New -->
          <table class="table align-items-center table-flush " id="tableDefaultSearchNew">
            <thead class="thead-light">
              <tr>
                <th scope="col" class="sort" data-sort="name">STT</th>
                <th scope="col" class="sort" data-sort="budget">TÊN KHÁCH</th>
                <th scope="col" class="sort" data-sort="status">SỐ ĐIỆN THOẠI</th>
                <th scope="col" class="sort" data-sort="completion">SỐ TIỀN MUA HÀNG</th>
                <th scope="col" class="sort" data-sort="completion">SỐ TIỀN CẦM ĐỒ</th>
                <th scope="col" class="sort" data-sort="completion">SỐ TIỀN CHUYỂN ĐỔI</th>
                <th scope="col" class="sort" data-sort="completion">SỐ TIỀN LÃI</th>
                <th class="text-center" scope="col" class="sort" data-sort="completion">TẶNG QUÀ</th>
              </tr>
            </thead>

            <!-- pagination -->
            <?php
            $t = query_select("SELECT customer.CustomerID, customer.CustomerName, customer.CustomerPhoneNumber, customer.CustomerAddress, SUM(orders.OrderTotal) AS OrderTotal FROM customer, orders WHERE customer.CustomerID = orders.CustomerID GROUP BY customer.CustomerID ORDER BY OrderTotal DESC");
            $total = $t->rowCount();
            $_SESSION['total'] = $total;

            $start = 0;
            //$limit = 20;
            $getIDpag = 1;

            if(isset($_GET['id']))
            {
              $id = $_GET['id'];
              $start = ($id-1)*$limit;
            }
            else
            {
              $id = 1;
            }
            $page = ceil($total/$limit);
            ?>

            <!-- get database news -->
            <?php
            $table = query_select("SELECT customer.CustomerID, customer.CustomerName, customer.CustomerPhoneNumber, customer.CustomerAddress, SUM(orders.OrderTotal) AS OrderTotal, SUM(orders.OrderInterestRate) AS OrderInterestRate FROM customer, orders WHERE customer.CustomerID = orders.CustomerID GROUP BY customer.CustomerID ORDER BY OrderInterestRate DESC limit $start, $limit");
            $count = $table->rowCount();
            
            // lấy cài đặt (số tiền tối thiểu để nhận quà)
            $settingRs = query_select("SELECT GiftCondition FROM Settings;");
            $settings = [];
            foreach($settingRs as $r){
              $settings = $r;
            }
            ?>

            <?php
            if($count > 0){
              $index = 0;
              foreach ($table as $row) {
                $customerID = $row["CustomerID"];
                // lấy số tiền mua hàng
                $temp = query_select("SELECT SUM(orders.OrderTotal) AS OrderTotal FROM customer, orders WHERE customer.CustomerID = orders.CustomerID AND customer.CustomerID = $customerID AND orders.OrderStatus = 1 LIMIT 1;");
                $totalBuy = [];
                foreach($temp as $r){
                  $totalBuy = $r;
                }
                // lấy số tiền cầm đồ
                $temp = query_select("SELECT SUM(orders.OrderTotal) AS OrderTotal FROM customer, orders WHERE customer.CustomerID = orders.CustomerID AND customer.CustomerID = $customerID AND orders.OrderStatus = 2 LIMIT 1;");
                $totalPawn = [];
                foreach($temp as $r){
                  $totalPawn = $r;
                }
                // lấy số tiền chuyển đổi
                $temp = query_select("SELECT SUM(orders.OrderTotal) AS OrderTotal FROM customer, orders WHERE customer.CustomerID = orders.CustomerID AND customer.CustomerID = $customerID AND orders.OrderStatus = 3 LIMIT 1;");
                $totalExchange = [];
                foreach($temp as $r){
                  $totalExchange = $r;
                }

                // kiểm tra đã nhận quà
                $checkReceivedGift = query_select("SELECT CustomerID FROM gift WHERE CustomerID=$customerID");
                ?>
                <tbody class="list">
                  <tr>
                    <td class="budget">
                      <?php echo $row['CustomerID']; ?>
                    </td>
                    <td class="budget">
                      <?php echo $row['CustomerName']; ?>
                    </td>
                    <td class="budget">
                      <?php echo empty($row['CustomerPhoneNumber']) ? "Không" : $row['CustomerPhoneNumber']; ?>
                    </td>
                    <td>
                      <?php echo number_format($totalBuy['OrderTotal'],0,".",",") . " ₫"; ?>
                    </td>
                    <td>
                      <?php echo number_format($totalPawn['OrderTotal'],0,".",",") . " ₫"; ?>
                    </td>
                    <td>
                      <?php echo number_format($totalExchange['OrderTotal'],0,".",",") . " ₫"; ?>
                    </td>
                    <td>
                      <?php echo number_format($row['OrderInterestRate'],0,".",",") . " ₫"; ?>
                    </td>
                    <td class="text-center">
                      <?php if($checkReceivedGift->rowCount() > 0) { ?> 
                          <span class="text-success">ĐÃ NHẬN QUÀ</span>
                        <?php } else if($row['OrderInterestRate'] < $settings["GiftCondition"]) { ?>
                          <span class="text-danger" title="Tổng tiền giao dịch phải từ <?php echo number_format($settings["GiftCondition"],0,".",",") ?> ₫">KHÔNG ĐỦ ĐIỀU KIỆN </br> NHẬN QUÀ</span>
                          <?php } else { ?> 
                        <a href="#" data-toggle="modal" data-target="#modal-giveAway" class="give-away" customer_id="<?php echo $row["CustomerID"]; ?>">
                          <button type="button" class="btn btn-sm btn-outline-info"> 
                          TẶNG QUÀ
                          </button>
                        </a>
                      <?php } ?>
                    </td>
                  </tr>
                </tbody>
                <?php
              }
            } else {
              ?>
              <tbody class="list">
                <tr>
                  <td class="text-center" colspan="6">
                    <div class="media-body">
                      <span class="name text-sm">Chưa có thông tin khách hàng
                        <a href="?page=add-news">TẠO MỚI THÔNG TIN KHÁCH HÀNG</a>
                      </span>
                    </div>
                  </td>
                </tr>
              </tbody>
              <?php
            } ?>
          </table>
        </div>

        <!-- show page -->
        <?php 
        if($count <= 20) {
          ?>
          <div class="card-footer py-4">
            <nav aria-label="...">
              <ul class="pagination justify-content-end mb-0">
                <?php if(isset($_GET['id'])) { $getIDpag = $_GET['id']; } ?>
                <?php if(isset($_SESSION['showEntries'])) { $showEntries = $_SESSION['showEntries']; } ?>

                <?php if($id > 1) {?>
                  <li class="page-item">
                    <a class="page-link" href="?page=list-gifts&&showEntries=<?php echo $showEntries; ?>&&id=<?php echo ($id-1); ?>" tabindex="-1">
                      <i class="fas fa-angle-left"></i>
                      <span class="sr-only">Trước</span>
                    </a>
                  </li>
                <?php } ?>

                <?php for($i=1;$i <= $page;$i++) {?>
                  <li class="page-item <?php if($getIDpag == $i){ echo 'active'; } ?>">
                    <a class="page-link" href="?page=list-gifts&&showEntries=<?php echo $showEntries; ?>&&id=<?php echo $i; ?>"><?php echo $i; ?></a>
                  </li>
                <?php }?>

                <?php if($id !=$page && $count > 0) {?>
                  <li class="page-item">
                    <a class="page-link" href="?page=list-gifts&&showEntries=<?php echo $showEntries; ?>&&id=<?php echo ($id+1); ?>">
                      <i class="fas fa-angle-right"></i>
                      <span class="sr-only">Sau</span>
                    </a>
                  </li>
                <?php }?>
              </ul>
            </nav>
          </div>
          <?php
        }
        ?> <!-- end pagination -->
      </div>
    </div>
  </div>

  <!-- Give away  -->
  <div class="modal fade" id="modal-giveAway" tabindex="-1" role="dialog" aria-labelledby="modal-giveAway" aria-hidden="true">
    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
      <div class="modal-content bg-gradient-info">
        <form id="give_away_form">
          <input type="hidden" id="customer_id" name="customer_id" value="0">
          <input type="hidden" id="note" name="note" value="">
          <div class="modal-header">
            <h6 class="modal-title" id="modal-title-giveAway">TẶNG QUÀ</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="py-3 text-center">
              <i class="fas fa-gift ni-3x"></i>
              <!-- <i class="fas fa-key ni-3x"></i> -->
            </div>
            
            <h3 class="text-center text-white">Tặng quà cho người này?</h3>
            
          </div>

          <div class="modal-footer">
            <button type="submit" name="submit_giveAway" id="submit_giveAway" class="btn btn-white">
              <i class="fas fa-hourglass autoLoadding" id="autoLoaddingUpGiveAway" style="display: none;"></i>
              <span id="updateNowGiveAway">OK</span>
            </button>
            <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Không</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Nguyễn Văn Tài</a>
        </div>
      </div>
    </div>
  </footer>
</div>