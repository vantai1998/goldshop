$(document).ready(function(){
	// pass customer id to form
	$('a.give-away').click(function() {
		let customerId = $(this).attr("customer_id");
		$('#give_away_form #customer_id').val(customerId);
	});

	// submit form
	$('#give_away_form').on('submit', function(event) {
		// $(this).hide();
		event.preventDefault();
		$.ajax({
			url: "./services/give-away.services.php",
			method: "POST",
			data: new FormData(this),
			// dataType : 'json',
			contentType:false,
			cache:false,
			processData:false,
			beforeSend: function() {
				// alert('prepare');
				// $('#submit_changePassword').attr('disabled', 'disabled');
				$('#autoLoaddingUpGiveAway').show();
				$('#updateNowGiveAway').text('Đang tải...');
			},
			success: function(res) {     
				if(res){
					location.reload();
				}
			},
			error: (err) => {
				console.log("ERROR AJAX");
				console.log(err);
			}
		});
	});
});