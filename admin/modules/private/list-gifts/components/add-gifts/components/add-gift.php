<!-- header form background -->
<div class="header pb-6 d-flex align-items-center" style="min-height: 200px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
	<span class="mask bg-gradient-default opacity-8"></span>
</div>

<!-- form submit -->
<div class="container-fluid mt--7">
	<div class="row">
		<div class="col-xl-12 order-xl-1">
			<div class="card">
				<!-- top form -->
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-8">
							<h3 class="mb-0">ADD NEWS </h3>
						</div>
						<div class="col-4 text-right">
							<a href="?page=list-news" class="btn btn-sm btn-primary">Back</a>
						</div>
					</div>
				</div>
				
				<!-- main form -->
				<div class="card-body">
					<form id="validate_form" enctype="multipart/form-data">
						<h6 class="heading-small text-muted mb-4">ADD NEWS</h6>
						<div class="pl-lg-4">
							<div class="row">
								<!-- Tiêu đề -->
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-tieude">TITLE</label>
										<input type="text" id="input-tieude" class="form-control" name="tieude" placeholder="Nhập tiêu đề tin..." 
										required data-parsley-pattern="^[a-zA-Z]+$" data-parsley-trigger="keyup">
									</div>
								</div>
								
								<!-- Tiêu đề không dấu -->
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="input-tieudekhongdau">TITLE DOES NOT SEAL</label>
										<input type="text" id="input-tieudekhongdau" class="form-control" name="tieudekhongdau" placeholder="Nhập tiêu đề không dấu..." required data-parsley-pattern="^[a-zA-Z]+$" data-parsley-trigger="keyup">
									</div>
								</div>

								<!-- Loại tin -->
								<div class="col-lg-3">
									<div class="form-group">
										<label for="exampleFormControlSelectLT">TYPE OF TIN</label>
										<select class="form-control" id="exampleFormControlSelectLT" required data-parsley-trigger="keyup" name="loaitin">
											<option value="">-- Chọn Loại Tin --</option>
											<?php 
											$table = query_select("SELECT idLT, Ten FROM loaitin WHERE loaitin.AnHien = 1");
											$count = $table->rowCount();
											if ($count > 0) {
												foreach ($table as $row) {
													?>
													<option value="<?php echo $row['idLT']; ?>"><?php echo $row['Ten'] ?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								
								<!-- Thể loại -->
								<div class="col-lg-3">
									<div class="form-group">
										<label for="exampleFormControlSelectTL">CATEGORY</label>
										<select class="form-control" id="exampleFormControlSelectTL" required data-parsley-trigger="keyup" name="theloai">
											<option value="">-- Chọn Thể Loại --</option>
											<?php 
											$table = query_select("SELECT idTL, TenTL FROM theloai WHERE theloai.AnHien = 1");
											$count = $table->rowCount();
											if ($count > 0) {
												foreach ($table as $row) {
													?>
													<option value="<?php echo $row['idTL']; ?>"><?php echo $row['TenTL'] ?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>

								<!-- Ngày đăng -->
								<div class="col-lg-3">
									<div class="form-group">
										<label for="example-datetime-local-input" class="form-control-label">DATE SUBMITTED</label>
										<input class="form-control" type="date" id="example-datetime-local-input" required data-parsley-trigger="keyup" name="ngaydang">
									</div>
								</div>

								<!-- Highlight -->
								<div class="col-lg-3">
									<div class="form-group">
										<label for="input-highlight">HIGHLIGHTS</label>
										<div class="row">
											<div class="col-12">
												<label class="custom-toggle">
													<input type="checkbox" id="input-highlight" name="input-highlight" value="1">
													<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
												</label>
											</div>
										</div>
									</div>
								</div>

								<!-- Link liên kết -->
								<div class="col-lg-12">
									<div class="form-group">
										<label class="form-control-label" for="input-linklienket">LINK</label>
										<input type="text" id="input-linklienket" class="form-control" name="linklienket" placeholder="Nhập đường dẫn liên kết" required data-parsley-trigger="keyup">
									</div>
								</div>
							</div>
						</div>

						<!-- Nội dung tóm tắt -->
						<div class="pl-lg-4">
							<div class="form-group">
								<label class="form-control-label">SUMMARY CONTENT</label>
								<textarea rows="4" class="form-control" placeholder="Nhập nội dung tóm tắt ..." required data-parsley-trigger="keyup" name="noidungtomtat"></textarea>
							</div>
						</div>

						<!-- Nội dung chính -->
						<div class="pl-lg-4">
							<div class="form-group">
								<label class="form-control-label">MAIN CONTENT</label>
								<textarea rows="4" class="form-control" placeholder="Nhập nội dung chính ..." required data-parsley-trigger="keyup" name="noidungchinh"></textarea>
							</div>
						</div>

						<div class="pl-lg-4">
							<div class="custom-file custom-uploadimg">
								<input type="file" class="custom-file-input" id="customFileLang" lang="en" required data-parsley-trigger="keyup" name="image[]" multiple accept=".jpg, .png, .gif">
								<label class="custom-file-label" for="customFileLang">Select file</label>
							</div>
						</div>
						
						<!-- Button submit -->
						<div class="pl-lg-4"> <br>
							<button type="submit" name="submit" id="submit_addNew" class="btn btn-primary btn-lg btn-block">
								<i class="fas fa-hourglass autoLoadding" id="autoLoaddingAddNew" style="display: none;"></i>
								<span id="addNowNew">Add New</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<footer class="footer pt-0">
		<div class="row align-items-center justify-content-lg-between">
			<div class="col-lg-6">
				<div class="copyright text-center  text-lg-left  text-muted">
					© 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
				</div>
			</div>
			<div class="col-lg-6">
				<ul class="nav nav-footer justify-content-center justify-content-lg-end">
					<li class="nav-item">
						<a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
					</li>
					<li class="nav-item">
						<a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
					</li>
					<li class="nav-item">
						<a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
					</li>
					<li class="nav-item">
						<a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
					</li>
				</ul>
			</div>
		</div>
	</footer>
</div>
