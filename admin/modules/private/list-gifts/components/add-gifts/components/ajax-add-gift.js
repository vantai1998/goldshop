$(document).ready(function(){
	$('#validate_form').parsley();
	$('#validate_form').on('submit', function(event) {
		event.preventDefault();

		if ($('#validate_form').parsley().isValid()) {
			$.ajax({
				url: "./services/list-news.services.php",
				method: "POST",
				data: new FormData(this),
				contentType:false,
				cache:false,
				processData:false,
				beforeSend: function() {
					$('#submit_addNew').attr('disabled', 'disabled');
					$('#autoLoaddingAddNew').show();
					$('#addNowNew').text('Loading...');
				},
				success: function(data) {
					$('#validate_form')[0].reset();
					$('#validate_form').parsley().reset();
					$('#submit_addNew').attr('disabled', false);
					$('#autoLoaddingAddNew').hide();
					$('#addNowNew').text('Add New');
					Swal.fire({
						type: 'success',
						title: 'ADD NEW',
						text: 'You have just added a successful data',
						showConfirmButton: false,
						timer: 1500
					});
				}
			});
		}
	});

});