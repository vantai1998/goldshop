<!-- header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Trang Chủ</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="./">Trang Quản Trị</a></li>
            </ol>
          </nav>
        </div>
      </div>

      <!-- Card stats -->
      <div class="row">
        <div class="col-xl-4 col-md-4">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Tổng tiền giao dịch</h5>
                  <?php
                  $totalRs = query_select("SELECT SUM(OrderTotal) AS Total FROM orders");
                  $totalAmount = 0;
                  foreach ($totalRs as $total){
                    $totalAmount += $total["Total"];
                  }
                  ?>
                  <span class="h2 font-weight-bold mb-0"><?php echo number_format($totalAmount * 1000,0,".",",") . " ĐỒNG"; ?></span>
                  <script>localStorage.setItem('total_new', <?php echo $totalAmount; ?>);</script>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="fas fa-money-check-alt"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-md-4">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">SỐ NGƯỜI GIAO DỊCH</h5>
                  <?php
                  $countTraders = query_select("SELECT CustomerID FROM customer");
                  ?>
                  <span class="h2 font-weight-bold mb-0"><?php echo $countTraders->rowCount(); ?></span>
                  <script>localStorage.setItem('count_CountTraders', <?php echo $countTraders->rowCount(); ?>);</script>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                    <i class="ni ni-chart-pie-35"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-md-4">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Tổng số giao dịch</h5>
                  <?php
                  $countTransactions = query_select("SELECT OrderID FROM orders");
                  ?>
                  <span class="h2 font-weight-bold mb-0"><?php echo $countTransactions->rowCount(); ?></span>
                  <script>localStorage.setItem('count_transaction', <?php echo $countTransactions->rowCount(); ?>);</script>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                    <i class="ni ni-chart-pie-35"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- table -->
<div class="container-fluid mt--6">

  <!-- top customers -->
  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">TOP KHÁCH HÀNG GIAO DỊCH NHIỀU</h3>
            </div>
          </div>
        </div>

        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">STT</th>
                <th scope="col">TÊN KHÁCH HÀNG</th>
                <th scope="col">SỐ TIỀN GIAO DỊCH</th>
              </tr>
            </thead>

            <!-- get database news -->
            <?php
            $table = query_select("SELECT customer.CustomerID, customer.CustomerName, customer.CustomerPhoneNumber, customer.CustomerAddress, SUM(orders.OrderTotal) AS OrderTotal FROM customer, orders WHERE customer.CustomerID = orders.CustomerID GROUP BY customer.CustomerID ORDER BY OrderTotal DESC limit 10");
            $count = $table->rowCount();
            ?>

            <?php
            if($count > 0){
              $index = 0;
              foreach ($table as $row) {
                ?>
                <tbody>
                  <tr>
                    <th scope="row">
                      <?php echo ++$index; ?>
                    </th>
                    <td>
                      <?php echo $row['CustomerName']; ?>
                    </td>
                    <td>
                      <?php echo number_format($row['OrderTotal'] * 1000,0,".",",") . " ₫"; ?>
                    </td>
                  </tr>
                </tbody>
                <?php
              }
            } else {
              ?>
              <tbody class="list">
                <tr>
                  <td class="text-center" colspan="6">
                    <div class="media-body">
                      <span class="name text-sm">No Page visits yet. 
                      </span>
                    </div>
                  </td>
                </tr>
              </tbody>
              <?php
            } ?>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Nguyễn Văn Tài</a>
        </div>
      </div>
    </div>
  </footer>
</div>