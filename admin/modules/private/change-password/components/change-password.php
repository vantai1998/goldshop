<div class="col-md-4">
    <div class="modal fade" id="modal-changePassword" tabindex="-1" role="dialog" aria-labelledby="modal-changePassword"
        aria-hidden="true">
        <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
            <div class="modal-content bg-gradient-info">
                <form id="val_form_changePassword">
                    <div class="modal-header">
                        <h6 class="modal-title" id="modal-title-changePassword">CẬP NHẬT MẬT KHẨU</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="py-3 text-center">
                            <i class="fas fa-key ni-3x"></i>
                        </div>

                        <!-- Old Password -->
                        <div class="form-group">
                            <label for="chg_old_password" class="form-control-label">Mật Khẩu Cũ</label>
                            <input class="form-control" type="password" id="chg_old_password" name="chg_old_password"
                                placeholder="Nhập mật khẩu cũ..." required data-parsley-trigger="keyup">
                        </div>

                        <!-- New Password -->
                        <div class="form-group">
                            <label for="chg_new_password" class="form-control-label">Mật Khẩu Mới</label>
                            <input class="form-control" type="password" id="chg_new_password" name="chg_new_password"
                                placeholder="Nhập mật khẩu mới..." required data-parsley-length="[8, 16]"
                                data-parsley-trigger="keyup">
                        </div>

                        <!-- Confirm Password -->
                        <div class="form-group">
                            <label for="chg_confirm_password" class="form-control-label">Nhập Lại Mật Khẩu</label>
                            <input class="form-control" type="password" id="chg_confirm_password"
                                name="chg_confirm_password" placeholder="Nhập lại mật khẩu..."
                                data-parsley-equalto="#chg_new_password" data-parsley-trigger="keyup" required>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit_changePassword" name="submit_changePassword" id="submit_changePassword"
                            class="btn btn-white">
                            <i class="fas fa-hourglass autoLoadding" id="autoLoaddingUpPass" style="display: none;"></i>
                            <span id="updateNowPass">CẬP NHẬT</span>
                        </button>
                        <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">ĐÓNG</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>