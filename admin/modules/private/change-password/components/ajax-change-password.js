$(document).ready(function(){
$('#val_form_changePassword').parsley();
$('#val_form_changePassword').on('submit', function(event) {
	event.preventDefault();

	if ($('#val_form_changePassword').parsley().isValid()) {
		$.ajax({
			url: "./services/change-password.services.php",
			method: "POST",
			data: new FormData(this),
			dataType : 'json',
			contentType:false,
			cache:false,
			processData:false,
			beforeSend: function() {
				$('#submit_changePassword').attr('disabled', 'disabled');
				$('#autoLoaddingUpPass').show();
				$('#updateNowPass').text('Đang xử lý...');
			},
			success: function(res) {
				if(res.check_password) {
					Swal.fire({
						type: 'success',
						title: 'SUCCESS',
						text: 'Cập Nhật Mật Khẩu Thành Công',
						showConfirmButton: false,
						timer: 1500
					});
					$('#val_form_changePassword')[0].reset();
					$('#val_form_changePassword').parsley().reset();
					$('#submit_changePassword').attr('disabled', false);
					$('#autoLoaddingUpPass').hide();
					$('#updateNowPass').text('CẬP NHẬT');
				} else {
					Swal.fire({
						type: 'error',
						title: 'Oops...',
						text: 'Mật Khẩu Cũ Không Đúng',
						showConfirmButton: false
					});
					$('#submit_changePassword').attr('disabled', false);
					$('#autoLoaddingUpPass').hide();
					$('#updateNowPass').text('CẬP NHẬT');
					$('#chg_old_password').focus();
				}
			}
		});
	}
});
});