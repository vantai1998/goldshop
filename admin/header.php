<!-- open session -->
<?php session_start(); ?>

<!-- set url -> login -->
<?php 
    // if(!isset($_SERVER['HTTP_REFERER'])){
    //     header('location:../index.php');
    // }    
?>

<!-- check login -->
<?php
    // check login role
    if(isset($_SESSION['role_admin'])){
        $_SESSION['isLoged'] = true;
    }

    // show title wellcome
    if($_SESSION['helloTitle'] == 1){
        $_SESSION['helloTitle'] = 0;
        echo '<script type="text/javascript">';
        echo "
        setTimeout(function () {
            Swal.fire({
              type: 'success',
              title: 'CHÀO MỪNG QUẢN TRỊ VIÊN',
              showConfirmButton: false,
              timer: 1500
          });";
          echo '}, 1500);</script>';
      }
  ?>

  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"  content="Tin Tức">
    <meta name="author" content="Creative Tim">
    <title>TRANG QUẢN TRỊ</title>
    <!-- Favicon -->
    <link rel="icon" href="./../assets/admin/img/brand/favicon.png" type="image/jpg">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="./../assets/admin/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="./../assets/admin/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <!-- Argon CSS -->
    <link rel="stylesheet" href="./../assets/admin/css/argon.css?v=1.2.0" type="text/css">
    <!-- Style format -->
    <link rel="stylesheet" href="./../assets/style.css" type="text/css">
    <!-- jquery min load -->
    <script src="./../assets/admin/vendor/jquery/dist/jquery.min.js"></script>
</head>