<!-- select -->
<?php include("../functions.php"); ?>

<!-- xóa ds đã tặng quà sau 30 ngày -->
<?php
  // lấy cài đặt (số tiền tối thiểu để nhận quà)
  // $settingRs = query_select("SELECT DeleteReceiveGiftTime FROM Settings;");
  // $settings = [];
  // if($settingRs->rowCount() > 0){
    // foreach($settingRs as $r) {
      // $settings = $r;
    // }
    // $sql = "DELETE FROM gift WHERE DATE(GiftDate) <= SUBDATE(CURRENT_DATE(), INTERVAL " . $settings['DeleteReceiveGiftTime'] . " DAY);";
    $sql = "DELETE FROM gift WHERE YEAR(DATE(GiftDate)) <> YEAR(CURRENT_DATE());";
    query_select($sql);
  // }
?>

<!-- header -->
<?php include("header.php"); ?>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="index.php">
          <img src="../assets/admin/GOLD.png" style="max-height: 5rem;" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <?php include("navabar.php"); ?>
        </div>
      </div>
    </div>
  </nav>

  <!-- Main content -->
  <?php include("main/main.php"); ?>

  <!-- footer -->
  <?php include("footer.php"); ?>
