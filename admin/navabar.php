<!-- create $page -->
<?php if(isset($_GET['page'])) { $page = $_GET['page']; }?>

<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link <?php if($page == 'list-news' || $page == 'add-news' || $page == 'detailnew') { echo 'active'; } ?>" href="?page=list-news">
      <i class="ni ni-single-02 text-yellow"></i>
      <span class="nav-link-text">KHÁCH HÀNG</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php if($page == 'list-gifts' || $page == 'add-gifts' || $page == 'detailgift') { echo 'active'; } ?>" href="?page=list-gifts">
      <i class="fas fa-gifts text-danger"></i>
      <span class="nav-link-text">DANH SÁCH TẶNG QUÀ</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php if($page == 'settings') { echo 'active'; } ?>" href="?page=settings">
      <i class="ni ni-settings text-primary"></i>
      <span class="nav-link-text">CÀI ĐẶT</span>
    </a>
  </li>
</ul>