//
// Bars chart
//

var BarsChart = (function() {

	//
	// Variables
	//

	var $chart = $('#chart-barsNew');

	// Bin data localStorage list new
	var total_new = localStorage.getItem('total_new');
	var count_view = localStorage.getItem('count_view');
	var count_fav = localStorage.getItem('count_fav');
	var count_eva = localStorage.getItem('count_eva');
	var count_cmt = 170;

	//
	// Methods
	//

	// Init chart
	function initChart($chart) {

		// Create chart
		var ordersChart = new Chart($chart, {
			type: 'bar',
			data: {
				labels: ['Total News', 'View', 'Favourite', 'Evalute', 'Hit Comments'],
				datasets: [{
					label: 'Sales',
					data: [total_new, count_view, count_fav, count_eva, count_cmt]
				}]
			}
		});

		// Save to jQuery object
		$chart.data('chart', ordersChart);
	}


	// Init chart
	if ($chart.length) {
		initChart($chart);
	}

})();

'use strict';


//
// Sales chart
//

var SalesChart = (function() {

  // Variables

  var $chart = $('#chart-sales-dark');


  // Methods

  function init($chart) {

  	var salesChart = new Chart($chart, {
  		type: 'line',
  		options: {
  			scales: {
  				yAxes: [{
  					gridLines: {
  						lineWidth: 1,
  						color: Charts.colors.gray[900],
  						zeroLineColor: Charts.colors.gray[900]
  					},
  					ticks: {
  						callback: function(value) {
  							if (!(value % 10)) {
  								return '$' + value + 'k';
  							}
  						}
  					}
  				}]
  			},
  			tooltips: {
  				callbacks: {
  					label: function(item, data) {
  						var label = data.datasets[item.datasetIndex].label || '';
  						var yLabel = item.yLabel;
  						var content = '';

  						if (data.datasets.length > 1) {
  							content += '<span class="popover-body-label mr-auto">' + label + '</span>';
  						}

  						content += '<span class="popover-body-value">$' + yLabel + 'k</span>';
  						return content;
  					}
  				}
  			}
  		},
  		data: {
  			labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  			datasets: [{
  				label: 'Performance',
  				data: [0, 20, 10, 30, 15, 40, 20, 60, 60]
  			}]
  		}
  	});

    // Save to jQuery object

    $chart.data('chart', salesChart);

};


  // Events

  if ($chart.length) {
  	init($chart);
  }

})();