// format currency
const input = document.querySelector("#type-service");
const [label] = input.labels;
const formatter = new Intl.NumberFormat("vi-VN", {style: "currency", currency: "VND"});

input.addEventListener("input", e => {
  label.textContent = formatter.format(input.value)
})

const input2 = document.querySelector("#type-service2");
const [label2] = input2.labels;
const formatter2 = new Intl.NumberFormat("vi-VN", {style: "currency", currency: "VND"});

input2.addEventListener("input", e => {
  label2.textContent = formatter2.format(input2.value)
})