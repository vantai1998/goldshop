$(document).ready(function() {
	/**
	 * METHOD: AUTH
	 * CHECK ROLE
	 *********************************/
	 $('#val_logForm').parsley();
	 $('#val_logForm').on('submit', function(event) {
	 	event.preventDefault();
	 	if ($('#val_logForm').parsley().isValid()) {
	 		$.ajax({
	 			url: "ajax.php",
	 			method: "POST",
	 			data: new FormData(this),
	 			dataType : 'json',
	 			contentType:false,
	 			cache:false,
	 			processData:false,
	 			beforeSend: function() {
	 				$('#lg_submit').attr('disabled', 'disabled');
	 				$('#autoLoaddingLogin').show();
	 				$('#loginNow').text('Đang xử lý...');
	 			},
	 			success: function(res) {
	 				if(res.check_email == false) {
						Swal.fire({
							type: 'error',
							title: 'Oops...',
							text: 'Tài khoản không đúng. Vui lòng kiểm tra lại!',
							showConfirmButton: false
						});
	 					$('#lg_submit').attr('disabled', false);
	 					$('#autoLoaddingLogin').hide();
	 					$('#loginNow').text('ĐĂNG NHẬP');
						$('#lg_email').focus();
	 				} else {
	 					if(res.check_login == false) {
	 						Swal.fire({
	 							type: 'error',
	 							title: 'Oops...',
	 							text: 'Mật khẩu không đúng. Vui lòng kiểm tra lại!',
	 							showConfirmButton: false
	 						});
	 						$('#lg_submit').attr('disabled', false);
	 						$('#autoLoaddingLogin').hide();
	 						$('#loginNow').text('ĐĂNG NHẬP');
	 						$('#lg_email').focus();
	 					} else {
	 						window.location = "admin/index.php";
	 					}
	 				}
	 				
	 			}
	 		});
	 	}
	 });

	 /**
	 * ******************************* END POST
	 */

});